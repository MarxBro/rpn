#!/usr/bin/perl

use strict;
use Data::Dumper;
use Getopt::Std;

$|++;
my %opts;
getopts( 'i:', \%opts );
my $in = $opts{i} || '3,20,*';
my @innie = split( ',', $in );
my @stack = ();

foreach my $e (@innie) {
    if ( $e eq '+' ) {
        my $a = pop @stack;
        my $b = pop @stack;
        push( @stack, $a + $b );
    }
    elsif ( $e eq '-' ) {
        my $a = pop @stack;
        my $b = pop @stack;
        push( @stack, $a - $b );
    }
    elsif ((( $e eq '*' ) || ( $e eq 'x' )) || ( $e eq 'X' )) {
        my $a = pop @stack;
        my $b = pop @stack;
        push( @stack, $a * $b );
    }
    elsif (( $e eq '/' ) || ( $e eq '%' )) {
        my $a = pop @stack;
        my $b  = pop @stack;
        push( @stack, $a / $b );
    }
    else { push( @stack, $e ); }
}

print Dumper(@stack)
